package com.kirillk.service;

import java.net.URISyntaxException;
import java.util.List;

public interface ConvertService {
  public void convertToCsv (List<StringBuilder> info) throws URISyntaxException;
}
