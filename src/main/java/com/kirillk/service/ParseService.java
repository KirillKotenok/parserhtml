package com.kirillk.service;

import java.io.IOException;

public interface ParseService {
  StringBuilder parse(String url) throws IOException;
}
