package com.kirillk.service.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.kirillk.service.ParseService;

import java.io.IOException;

public class ModaParseServiceImpl implements ParseService {

  private int pageNum = 1;
  private int count = 1;
  private boolean isReady = false;
  private StringBuilder sb = new StringBuilder();

  @Override
  public StringBuilder parse(String url) throws IOException {
    while (!isReady) {
      doParse(url.substring(0, url.lastIndexOf(String.valueOf(1))) + pageNum++);
    }
    return sb;
  }

  private void doParse(String url) throws IOException {
    Document d = Jsoup.connect(url).get();

    Elements e = d.select("div.mp7g_oh.mjyo_6x.mse2_k4.m7er_k4.m09p_k4._1l8iq");
    for (Element el : e) {
      Elements spanWithDiscount = el.select("div.mh36_8.mvrt_8.mj7a_8.mg9e_8 >" +
              "div.mj7a_4.mjyo_lo._9c44d_jt8SR >" +
              "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3_DDQ.mpof_vs._9c44d_2MDwk >" +
              "span.mpof_uk.mqu1_ae._9c44d_18kEF.m9qz_yp._9c44d_2BSa0._9c44d_KrRuv");

      if (spanWithDiscount != null && spanWithDiscount.size() > 0) {

        String offerta = el.select("div.mh36_8.mg9e_8 > div._1y62o.mpof_ki._9c44d_3SD3k").text();
        String offerUrl = el.select("div.mjyo_lo.meqh_en.mp4t_0.m3h2_56.mryx_0.munh_56.m7er_56.mh36_8.mvrt_8.mj7a_8.mg9e_8._9c44d_1Yxex " +
                "> div.mpof_ki.mse2_k4.m7er_k4" +
                "> div.meqh_en.m7er_k4._9c44d_3_fkD" +
                "> div.msts_9u.mp7g_oh.mse2_k4.m7er_k4._9c44d_2Narh > a").attr("href");
        String offerImgUrl = el.select("div.mjyo_lo.meqh_en.mp4t_0.m3h2_56.mryx_0.munh_56.m7er_56.mh36_8.mvrt_8.mj7a_8.mg9e_8._9c44d_1Yxex " +
                "> div.mpof_ki.mse2_k4.m7er_k4" +
                "> div.meqh_en.m7er_k4._9c44d_3_fkD" +
                "> div.msts_9u.mp7g_oh.mse2_k4.m7er_k4._9c44d_2Narh " +
                "> a" +
                "> ul.mpof_ki.m389_6m.m7f5_5x.mse2_k4.m7er_k4.mp4t_0.m3h2_0.mryx_0.munh_0.mg9e_0.mvrt_0.mj7a_0.mh36_0._9c44d_rC-QB " +
                "> li.mpof_ki.m389_6m.m7f5_6m.mse2_k4.m7er_k4.mp4t_0.m3h2_0.mryx_0.munh_0.mg9e_0.mvrt_0.mj7a_0.mh36_0._9c44d_QEuaw" +
                "> img").attr("src");
        String offerName = el.select("div.mh36_8.mvrt_8.mj7a_8.mg9e_8 >" +
                "div.mj7a_4.mjyo_lo.mqu1_16.mgn2_14 >" +
                "h2.mgn2_14.m9qz_yp.mqu1_16.mp4t_0.m3h2_0.mryx_0.munh_0>" +
                "a._w7z6o._uj8z7.meqh_en.mpof_z0.mqu1_16._9c44d_2vTdY.m7er_k4.msa3_z4").text();
        String currentPrice = el.select("div.mh36_8.mvrt_8.mj7a_8.mg9e_8>" +
                "div.mj7a_4.mgn2_23>" +
                "div.mpof_92.mgn2_23>" +
                "div.msa3_z4._9c44d_2K6FN>" +
                "span._1svub._lf05o").text();

        double current = Double.parseDouble(currentPrice.substring(0, currentPrice.lastIndexOf("z") - 1).replaceAll("\\s+", "").replaceAll(",", "."));
        String oldPrice = el.select("div.mh36_8.mvrt_8.mj7a_8.mg9e_8 >" +
                "div.mj7a_4.mjyo_lo._9c44d_jt8SR >" +
                "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3_DDQ.mpof_vs._9c44d_2MDwk >" +
                "span.mpof_uk.mqu1_ae._9c44d_18kEF.m9qz_yp._9c44d_2BSa0._9c44d_KrRuv").text();

        double old = Double.parseDouble(oldPrice.substring(0, oldPrice.lastIndexOf("z") - 1).replaceAll("\\s+", "").replaceAll(",", "."));
        String delivery = el.select("div.mh36_8.mvrt_8.mj7a_8.mg9e_8 >" +
                "div._1y62o>" +
                "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3_DDQ>" +
                "span.mpof_uk.mqu1_ae._9c44d_18kEF._9c44d_3gH36.m9qz_yq").text();


        sb.append(!offerta.isEmpty());
        sb.append(",");
        sb.append(offerUrl);
        sb.append(",");
        sb.append(offerImgUrl);
        sb.append(",");
        sb.append(offerName);
        sb.append(",");
        sb.append(current);
        sb.append(",");
        sb.append(old);
        sb.append(",");
        sb.append(delivery);
        sb.append(",");
        sb.append("Moda");
        sb.append("\n");

        if (count == 100) {
          break;
        }
        count++;
      }
    }
    if (count >= 100) {
      isReady = true;
    }
  }
}
