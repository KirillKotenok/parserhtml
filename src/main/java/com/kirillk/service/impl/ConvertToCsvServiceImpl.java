package com.kirillk.service.impl;

import com.kirillk.service.ConvertService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.List;

public class ConvertToCsvServiceImpl implements ConvertService {
  private StringBuilder sb = new StringBuilder();

  @Override
  public void convertToCsv(List<StringBuilder> info) throws URISyntaxException {
    String path = ConvertToCsvServiceImpl.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
    File root = new File(path.substring(0, path.lastIndexOf("/")));
    addTableTitle();
    for (StringBuilder s : info) {
      sb.append(s);
    }
    try (PrintWriter writer = new PrintWriter(new File(root, "product.csv"))) {
      System.out.println(root+"/product.csv");
      writer.write(sb.toString().replaceAll("[\"]", ""));
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }

  private void addTableTitle() {
    sb.append("Offerta sponsorova");
    sb.append(",");
    sb.append("Offer url");
    sb.append(",");
    sb.append("Offer img url");
    sb.append(",");
    sb.append("Offer name");
    sb.append(",");
    sb.append("Current price");
    sb.append(",");
    sb.append("Old price");
    sb.append(",");
    sb.append("Delivery");
    sb.append(",");
    sb.append("Category");
    sb.append("\n");
  }
}


