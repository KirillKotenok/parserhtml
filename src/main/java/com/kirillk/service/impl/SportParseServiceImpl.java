package com.kirillk.service.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.kirillk.service.ParseService;

import java.io.IOException;

public class SportParseServiceImpl implements ParseService {

  private int pageNum = 1;
  private int count = 1;
  private boolean isReady = false;
  private StringBuilder sb = new StringBuilder();

  @Override
  public StringBuilder parse(String url) throws IOException {
    while (!isReady) {
      doParse(url.substring(0, url.lastIndexOf(String.valueOf(1))) + pageNum++);
    }
    return sb;
  }

  private void doParse(String url) throws IOException {
    Document d = Jsoup.connect(url).get();

    Elements e = d.select("div.mpof_ki.mqen_m6.mp7g_oh.mh36_0.mvrt_0.mg9e_8.mj7a_8.m7er_k4._1y62o._9c44d_1I1gg");
    for (Element el : e) {
      Elements spanWithDiscount = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq >" +
              "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3_DDQ.mpof_vs._9c44d_2MDwk >" +
              "span.mpof_uk.mqu1_ae._9c44d_18kEF.m9qz_yp._9c44d_2BSa0._9c44d_KrRuv");

      if (spanWithDiscount != null && spanWithDiscount.size() > 0) {
        String offerta = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq >" +
                "div.m7er_k4._9c44d_3TzmE >" +
                "div._1y62o.mpof_ki._9c44d_3SD3k").text();

        String offerUrl = el.select("div.mpof_ki.myre_zn.m389_6m.m09p_k4.mse2_56._9c44d_2Tos9 " +
                "> a.msts_9u.mg9e_0.mvrt_0.mj7a_0.mh36_0.mpof_ki.m389_6m.mx4z_6m.m7f5_6m.mse2_k4.m7er_k4._9c44d_1ILhl")
                .attr("href");

        String offerImgUrl = el.select("div.mpof_ki.myre_zn.m389_6m.m09p_k4.mse2_56._9c44d_2Tos9 " +
                "> a.msts_9u.mg9e_0.mvrt_0.mj7a_0.mh36_0.mpof_ki.m389_6m.mx4z_6m.m7f5_6m.mse2_k4.m7er_k4._9c44d_1ILhl" +
                "> img").attr("src");

        String offerName = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq >" +
                "div.m7er_k4._9c44d_3TzmE >" +
                "h2.mgn2_14.m9qz_yp.mqu1_16.mp4t_0.m3h2_0.mryx_0.munh_0>" +
                "a._w7z6o._uj8z7.meqh_en.mpof_z0.mqu1_16._9c44d_2vTdY  ").text();

        String currentPrice = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq>" +
                "div._9c44d_3AMmE>" +
                "div.mpof_92.myre_zn>" +
                "div.msa3_z4._9c44d_2K6FN>" +
                "span._1svub._lf05o").text();
        double current = Double.parseDouble(currentPrice.substring(0, currentPrice.lastIndexOf("z") - 1).replaceAll("\\s+", "").replaceAll(",", "."));

        String oldPrice = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq >" +
                "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3_DDQ.mpof_vs._9c44d_2MDwk >" +
                "span.mpof_uk.mqu1_ae._9c44d_18kEF.m9qz_yp._9c44d_2BSa0._9c44d_KrRuv").text();

        double old = Double.parseDouble(oldPrice.substring(0, oldPrice.lastIndexOf("z") - 1).replaceAll("\\s+", "").replaceAll(",", "."));

        String delivery = el.select("div.mpof_ki.myre_zn._9c44d_1Hxbq >" +
                "div._9c44d_3K52C>" +
                "div.mp0t_ji.mpof_vs._9c44d_1VS-Y._9c44d_3px8G>" +
                "span.mpof_uk.mqu1_ae._9c44d_18kEF._9c44d_3gH36.m9qz_yq").text();

        sb.append(!offerta.isEmpty());
        sb.append(",");
        sb.append(offerUrl);
        sb.append(",");
        sb.append(offerImgUrl);
        sb.append(",");
        sb.append(offerName);
        sb.append(",");
        sb.append(current);
        sb.append(",");
        sb.append(old);
        sb.append(",");
        sb.append(delivery);
        sb.append(",");
        sb.append("Sport");
        sb.append("\n");

        if (count == 100) {
          break;
        }
        count++;
      }
    }
    if (count >= 100) {
      isReady = true;
    }
  }
}