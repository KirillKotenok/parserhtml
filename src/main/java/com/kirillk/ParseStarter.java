package com.kirillk;

import com.kirillk.service.ConvertService;
import com.kirillk.service.ParseService;
import com.kirillk.service.impl.ConvertToCsvServiceImpl;
import com.kirillk.service.impl.ElectronicParseServiceImpl;
import com.kirillk.service.impl.ModaParseServiceImpl;
import com.kirillk.service.impl.SportParseServiceImpl;

import java.util.List;

public class ParseStarter {

  public final static void main(String[] args) throws Exception {
    String categoryModa = "https://allegro.pl/kategoria/moda?string=bargain_zone" +
            "&bmatch=e2101-d3718-c3682-fas-1-2-0304&p=1";
    String categoryElectronic = "https://allegro.pl/kategoria/elektronika?string=bargain_zone" +
            "&bmatch=cl-e2101-d3718-c3682-ele-1-2-0304&p=1";
    String categorySport = "https://allegro.pl/kategoria/sport-i-turystyka?string=bargain_zone" +
            "&bmatch=e2101-d3718-c3682-spo-1-2-0304&p=1";
    ParseService parseServiceModa = new ModaParseServiceImpl();
    ParseService parseServiceSport = new SportParseServiceImpl();
    ParseService parseServiceElectronic = new ElectronicParseServiceImpl();

    StringBuilder builderModa = parseServiceModa.parse(categoryModa);
    StringBuilder builderSport = parseServiceSport.parse(categorySport);
    StringBuilder builderElectronic = parseServiceElectronic.parse(categoryElectronic);

    ConvertService cs = new ConvertToCsvServiceImpl();
    cs.convertToCsv(List.of(builderSport, builderElectronic, builderModa));
  }
}
